#!/bin/bash

_extras=wallpapers
_repo="Peppermint_OS/PepWallPaper"

###########################################
# Quietly have wget -q0- "pipe" its output directly to tar.
# Use tar -xvz to decompress, list and untar the DL file
# to the symlink 'pepwallpaper' in /usr/share, on the fly.
# Leaving no files to remove and no permission changes required.
###########################################

echo -e "\n Downloadng Bonus ${_extras} to /usr/share/"
wget https://codeberg.org/${_repo}/archive/master.tar.gz --show-progress -qO - |
  tar -xz -C /usr/share 2>/dev/null 

echo  -e "\n Bonus Wallpapers downloaded and installed.\n"

read -n1 -p " Process Completed. Press any key to close this dialog." answ 

# Go back to the Extras
python3 /opt/pypep/pge.py &

